/**
 * Created by jbarron on 3/13/16.
 */
function smallModal(message){
    $('#smallModal').modal('show');
    $('#smallModalContent').text(message);
}

/**
 * Created by jbarron on 3/13/16.
 */
function smallModalHide(message){
    $('#smallModal').modal('hide');
}