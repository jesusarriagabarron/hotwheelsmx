<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\Auth\AuthMongoController;

class AuthMongoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!AuthMongoController::check()){

            if ($request->ajax() || $request->wantsJson()) {
                return response('Tu sesion ha caducado, por favor logueate nuevamente.', 401);
            }


            return redirect()->route('logInPage');
        }
        return $next($request);
    }
}
