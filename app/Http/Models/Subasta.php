<?php

/**
 * Created by PhpStorm.
 * User: jbarron
 * Date: 6/10/16
 * Time: 12:07 PM
 */

namespace App\Http\Models;

use App\Http\Controllers\Auth\AuthMongoController;

use Illuminate\Support\Facades\App;

Abstract class Subasta
{
	const HOTWHEELS = 1;
	const MATCHBOX	=	2;
	const M2	=	3;
	const MAISTO = 4;
	const AUTOWORLD = 5;
	const JOHNNY_LIGHTNING = 6;
	const GREENLIGHT = 7;
	const JADA	=	8;
	const OTRO = 99;

	static public $marcas = array(
		Subasta::HOTWHEELS	=>	'Hotwheels',
		Subasta::MATCHBOX	=>	'Matchbox',
		Subasta::M2	=>	'M2',
		Subasta::MAISTO	=>	'Maisto',
		Subasta::AUTOWORLD	=> 'Autoworld',
		Subasta::JOHNNY_LIGHTNING => 'Johnny Lightning',
		Subasta::GREENLIGHT => 'Greenlight',
		Subasta::JADA	=>	'Jada',
		Subasta::OTRO	=>	'Otro'
	);

	const STATUS_PAUSED = 0;
	const STATUS_IN_PROGRESS =1;
	const STATUS_FINISHED = 2;
	const STATUS_COMPLETED = 3;
	const STATUS_CREATED = 4;

	static public $status = array (
		Subasta::STATUS_PAUSED => 'Pausada',
		Subasta::STATUS_IN_PROGRESS => 'En Progreso',
		Subasta::STATUS_FINISHED => 'Finalizada',
		Subasta::STATUS_COMPLETED => 'Completada',
		Subasta::STATUS_CREATED => 'Próximamente',
	);


	const PUJA_LIBRE = 1;
	const PUJA_FIJA = 2;

	protected $subastaODM;

	/**
	 * Metodo que crea una puja nueva y la asigna a la subasta
	 * @param $monto
	 * @return Puja|bool
	 */
	protected function crearPuja($monto=0)
	{

		try {
			$ODM = App::make('ODM');
			$puja = new \App\Http\Odm\Documents\Puja();
			$puja->setMontoPuja($monto);
			$puja->setSubasta($this->subastaODM);
			$puja->setIdUsuario(AuthMongoController::user()->getId());
			$puja->setCreatedAt();
			$ODM->persist($puja);
			$this->subastaODM->setPujas($puja);
			$ODM->persist($this->subastaODM);
			$ODM->flush();
			return $puja;
		}catch(\Error $e){
			return false;
		}
	}

	/**
	 * Metodo que regresa la Subasta para ser usada
	 * @param $data
	 * @return \App\Http\Odm\Documents\Subasta|bool
	 */
	protected function crearSubasta($data)
	{

		try {
			$ODM = App::make('ODM');

			$foto = new \App\Http\Odm\Documents\FotoAuto();
			$foto->setFilename($data['filename']);


			$auto = new \App\Http\Odm\Documents\Auto();
			$auto->setDescripcion($data['descripcion']);
			$auto->setEmpaque($data['empaque']);
			$auto->setFotos($foto);
			$auto->setMarca($data['marca']);
			$auto->setNombre($data['nombre']);

			$usuario =  $ODM->getRepository('App\Http\Odm\Documents\Usuario')->findOneBy(['id'=>AuthMongoController::user()->getId()]);


			$subasta = new \App\Http\Odm\Documents\Subasta();
			$subasta->setPrecioInicial($data['precioInicial']);
			$subasta->setInicioSubasta($data['inicioDateTime']);
			$subasta->setFinSubasta($data['finDateTime']);
			$subasta->setReglasAdicionales($data['reglas']);
			$subasta->setTipoPuja($data['tipoPuja']);
			$subasta->setIncrementoPuja($data['incrementoPuja']);
			$subasta->setStatus(Subasta::STATUS_CREATED);
			$subasta->setUsuario($usuario);
			$auto->setSubasta($subasta);

			$ODM->persist($subasta);
			$ODM->persist($auto);
			$ODM->persist($usuario);

			$ODM->flush();

			return $subasta;

		}catch(\Error $e){
			return false;
		}
	}

	/**
	 * Metodo para obtener la lista de subastas
	 * @return mixed
	 */
	public static function getSubastas()
	{
		$ODM	=	App::make('ODM');
		$subastas =	$ODM->createQueryBuilder('App\Http\Odm\Documents\Subasta')
			->sort('id','desc')
			->getQuery()
			->execute();
		return $subastas;
	}




	/**
	 * Metodo para obtener la lista de subastas
	 * @return mixed
	 */
	public static function getSubastasEnProgreso()
	{
		$ODM	=	App::make('ODM');
		$subastas =	$ODM->getRepository('App\Http\Odm\Documents\Subasta')->findBy(array('status'=>Subasta::STATUS_IN_PROGRESS));
		return $subastas;
	}


	public static function getSubastasPorEmpezar()
	{
		$ODM	=	App::make('ODM');
		$subastas = $ODM->getRepository('App\Http\Odm\Documents\Subasta')->findBy(array('status'=>Subasta::STATUS_CREATED));
		return $subastas;
	}



	/**
	 * Metodo que nos regresa el objeto de una subasta especifica
	 * @param $id
	 * @return null
	 */
	public static function getSubasta($id)
	{
		$ODM	=	App::make('ODM');
		$subasta	=	$ODM->getRepository('App\Http\Odm\Documents\Subasta')->findBy(array('id'=>$id));
		if(!$subasta)
			return null;

		return	$subasta[0];
	}



}