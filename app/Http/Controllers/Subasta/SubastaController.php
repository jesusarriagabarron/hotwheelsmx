<?php
/**
 * Created by PhpStorm.
 * User: jbarron
 * Date: 3/23/16
 * Time: 9:45 PM
 */

namespace App\Http\Controllers\Subasta;


use App\Http\Controllers\Auth\AuthMongoController;
use App\Http\Controllers\Controller;
use App\Http\Models\Subasta;
use App\Http\Models\SubastaInglesa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Response;


class SubastaController extends Controller
{
	public function crearSubasta()
	{
		return view('subasta.crearSubasta');
	}
	/**
	 * Metodo que recibe la request usando POST para crear una subasta
	 * @param Request $request
	 * @return $this
	 */
	public function postCrearSubasta(Request $request)
	{
		$rules = [
			'nombre' => 'required',
			'foto'	=>	'image',
			'inicioDateTime' => 'required',
			'finDateTime' => 'required',
			'precioInicial' => 'required|numeric',
			'incrementoPuja' => 'numeric',
		];

		$validator = Validator::make($request->all(), $rules);

		if($validator->fails())
			return  redirect()->route('crearSubastaPage')->withErrors($validator)->withInput();

		$fileName =	'auto-'.str_random(8).'.jpg';
		$manager    =   new ImageManager();
		$request->file('foto')->move(storage_path().'/app/public/autos/', 'temp-'.$fileName);

		$image      =   $manager->make(storage_path().'/app/public/autos/temp-'.$fileName)->orientate();
		$image->widen(600)->text('www.hotwheelsMX.com',10,10,function($font){
			$font->size(20);
		});

		Storage::disk('local')->put('public/autos/'.$fileName,$image->stream());
		Storage::disk('local')->delete('public/autos/temp-'.$fileName);
		Storage::disk('local')->put('public/autos/thumb-'.$fileName,$image->fit(300)->stream());

		$data =	[
			'nombre' => $request->input('nombre',null),
			'descripcion' => $request->input('descripcion',null),
			'marca' => $request->input('marca',null),
			'empaque' => $request->input('empaque',null),
			'filename'	=>	$fileName,
			'inicioDateTime' => $request->input('inicioDateTime',null),
			'finDateTime' => $request->input('finDateTime',null),
			'tipoPuja' => $request->input('tipoPuja',null),
			'precioInicial' => $request->input('precioInicial',null),
			'incrementoPuja' => $request->input('incrementoPuja',null),
			'reglas' => $request->input('reglas',null)
		];

		$subastaInglesa = new SubastaInglesa();

		if(!$subastaInglesa->crearSubasta($data)){
			Session::flash('notify',['type'=>'error', 'text'=>'Hubo un error al intentar crear la subasta']);
			return redirect()->route('crearSubastaPage')->withInput();
		}

		Session::flash('notify',['type'=>'success', 'text'=>'La subasta ha sido creada exitosamente']);
		return redirect()->route('crearSubastaPage');
	}


	/**
	 * Metodo que muestra una subasta en particular
	 * @param $auto
	 * @param $id
	 * @return $this|void
	 */
	public function verSubasta($auto,$id)
	{
		$rules	=	['id' => 'alpha_num'];
		$validator	=	Validator::make(['id'=>$id],$rules);
		$subasta = Subasta::getSubasta($id);
		if($validator->fails() || !$subasta)
			return abort(404);

		return view('subasta.verSubasta')->with(['subasta'=>$subasta]);
	}

	/**
	 * Metodo que recibe el post para crear una nueva puja
	 * en una subasta determinada por $id
	 * @param $id
	 * @param null $monto
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postMio($id,$monto=0)
	{
		$rules	=	['id' => 'alpha_num'];
		$validator	=	Validator::make(['id'=>$id],$rules);
		// OJO se carga la subasta primero antes de accesar al objeto
		$subasta = new SubastaInglesa(Subasta::getSubasta($id));
		if($validator->fails() || !$subasta)
			return response()->json(['error'=>'not found'],404);

		if($subasta->haFinalizado())
			return response(['message' => 'La subasta ha finalizado','reload'=>1],404);

		if($subasta->ultimaPujaEsMia())
			return response(['message'=>'No es necesario hacer una puja, tu eres el ganador por el momento'],Response::HTTP_BAD_REQUEST);

		if(!$puja = $subasta->crearPuja($monto))
			return response(['message'=>'la puja no pudo ser creada'],404);


		$pusher	=	App::make('pusher');
		$pusher->trigger( 'subasta-'.$id.'-channel',
			'nueva-puja',
			array( 	'usuario'=> AuthMongoController::user()->getFirstname(),
					'idUsuario' => AuthMongoController::user()->getId(),
				   	'montoPuja' => $puja->getMontoPuja(),
				   	'totalPujas'=> Subasta::getSubasta($id)->getTotalPujas(),
					'timePuja' =>  $puja->getCreatedAtDateFormat(),
					'mioPor'  =>  (($puja->getMontoPuja())+(Subasta::getSubasta($id)->getIncrementoPuja())),
			));

		return response(['pujaId'=>$puja->getId()]);
	}
}