<?php
/**
 * Created by PhpStorm.
 * User: jbarron
 * Date: 6/16/16
 * Time: 5:14 PM
 */

namespace App\Http\Odm\Documents;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations\ReferenceOne;
use Illuminate\Support\Facades\App;

/** @ODM\Document */
class Puja
{

	/** @ODM\Id */
	private $id;
	/** @ODM\Field(type="date") */
	private $createdAtDate;
	/** @ReferenceOne(targetDocument="App\Http\Odm\Documents\Subasta",  inversedBy="pujas") */
	private $subasta;

	/** @ODM\Field(type="integer") */
	private $montoPuja;

	/** @ODM\Field(type="string") */
	private $idUsuario;
	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}
	/**
	 * @return mixed
	 */
	public function getCreatedAtDate()
	{
		return $this->createdAtDate->format('Y-m-d H:i:s.u');
	}


	public function getCreatedAtDateFormat()
	{
		return $this->createdAtDate;
	}

	public function getMilliseconds(){
		return	$this->createdAtDate->format('u');
	}

	/**
	 * @param mixed $createdAtDate
	 */
	public function setCreatedAtDate()
	{
		list($msec,$sec) = explode(" ",microtime());
		list($zero,$mili) = explode(".",$msec);
		$d = new \DateTime(str_replace(" ","T",date('Y-m-d H:i:s')).'.'.$mili.'Z');
		$this->createdAtDate = $d->format('Y-m-d\TH:i:s.u');
	}


	public function setCreatedAt()
	{
		$this->setCreatedAtDate();
	}

	/**
	 * @return mixed
	 */
	public function getSubasta()
	{
		return $this->subasta;
	}

	/**
	 * @param mixed $subasta
	 */
	public function setSubasta($subasta)
	{
		$this->subasta = $subasta;
	}

	/**
	 * @return mixed
	 */
	public function getIdUsuario()
	{
		return $this->idUsuario;
	}

	/**
	 * @param mixed $idUsuario
	 */
	public function setIdUsuario($idUsuario)
	{
		$this->idUsuario = $idUsuario;
	}


	public function getUserPuja()
	{
		$ODM = App::make('ODM');
		$usuario = $ODM->getRepository('App\Http\Odm\Documents\Usuario')->find(['id'=>$this->idUsuario]);
		return $usuario;
	}

	/**
	 * @return mixed
	 */
	public function getMontoPuja()
	{
		return $this->montoPuja;
	}

	/**
	 * @param mixed $montoPuja
	 */
	public function setMontoPuja($montoPuja)
	{
		$this->montoPuja = $montoPuja;
	}



}