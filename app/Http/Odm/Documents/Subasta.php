<?php
/**
 * Created by PhpStorm.
 * User: jbarron
 * Date: 6/16/16
 * Time: 5:14 PM
 */

namespace App\Http\Odm\Documents;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations\ReferenceOne;
use Doctrine\ODM\MongoDB\Mapping\Annotations\ReferenceMany;

/** @ODM\Document */
class Subasta
{

	/** @ODM\Id */
	private $id;

	/** @ODM\Field(type="date") */
	private $inicioSubasta;

	/** @ODM\Field(type="date") */
	private $finSubasta;

	/** @ODM\Field(type="integer") */
	private $precioInicial;

	/** @ODM\Field(type="integer") */
	private $tipoPuja;

	/** @ODM\Field(type="string") */
	private $reglasAdicionales;

	/** @ODM\Field(type="int") */
	private $incrementoPuja;

	/** @ODM\Field(type="int") */
	private $status;

	/** @ReferenceMany(targetDocument="App\Http\Odm\Documents\Auto", mappedBy="subasta") */
	private $autos = array();

	/** @ReferenceMany(targetDocument="App\Http\Odm\Documents\Puja", mappedBy="subasta", cascade={"persist"}) */
	private $pujas = array();

	/**
	 * @ReferenceMany(
	 *      targetDocument="App\Http\Odm\Documents\Puja",
	 *      mappedBy="subasta",cascade={"persist"},
	 *      sort={"createdAtDate"="desc"}
	 * )
	 */
	private $pujasDesc;


	/** @ReferenceOne(targetDocument="App\Http\Odm\Documents\Usuario",  cascade={"persist"}, inversedBy="subastas") */
	private $usuario;


	public function __construct(){
		$this->autos = new ArrayCollection();
		$this->pujas = new ArrayCollection();
	}

	/**
	 * @return mixed
	 */
	public function getIncrementoPuja()
	{
		return $this->incrementoPuja;
	}

	/**
	 * @param mixed $incrementoPuja
	 */
	public function setIncrementoPuja($incrementoPuja)
	{
		$this->incrementoPuja = $incrementoPuja;
	}

	/**
	 * @return mixed
	 */
	public function getUsuario()
	{
		return $this->usuario;
	}

	/**
	 * @param mixed $usuario
	 */
	public function setUsuario($usuario)
	{
		$this->usuario = $usuario;
	}





	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getInicioSubasta()
	{
		return $this->inicioSubasta;
	}

	/**
	 * @param mixed $inicioSubasta
	 */
	public function setInicioSubasta($inicioSubasta)
	{
		$this->inicioSubasta = $inicioSubasta;
	}

	/**
	 * @return mixed
	 */
	public function getFinSubasta()
	{
		return $this->finSubasta;
	}

	/**
	 * @param mixed $finSubasta
	 */
	public function setFinSubasta($finSubasta)
	{
		$this->finSubasta = $finSubasta;
	}

	/**
	 * @return mixed
	 */
	public function getPrecioInicial()
	{
		return $this->precioInicial;
	}

	/**
	 * @param mixed $montoInicial
	 */
	public function setPrecioInicial($precioInicial)
	{
		$this->precioInicial = $precioInicial;
	}

	/**
	 * @return mixed
	 */
	public function getTipoPuja()
	{
		return $this->tipoPuja;
	}

	/**
	 * @param mixed $tipoPuja
	 */
	public function setTipoPuja($tipoPuja)
	{
		$this->tipoPuja = $tipoPuja;
	}

	/**
	 * @return mixed
	 */
	public function getReglasAdicionales()
	{
		return $this->reglasAdicionales;
	}

	/**
	 * @param mixed $reglasAdicionales
	 */
	public function setReglasAdicionales($reglasAdicionales)
	{
		$this->reglasAdicionales = $reglasAdicionales;
	}

	/**
	 * @return mixed
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param mixed $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return mixed
	 */
	public function getAutos()
	{
		return $this->autos;
	}

	/**
	 * @param mixed $autos
	 */
	public function setAutos($auto)
	{
		$this->autos[] = $auto;
	}

	public function getFirstAuto()
	{
		return	$this->autos[0];
	}

	public function getTotalPujas()
	{
		return count($this->pujas);
	}

	/**
	 * @return mixed
	 */
	public function getPujas($desc=0)
	{
		if(!$desc)
			return $this->pujas;

		return $this->pujasDesc;
	}

	public function getPujaMasAlta()
	{

		if(count($this->pujasDesc)>0)
			return $this->pujasDesc[0];
		else
			return false;
	}

	/**
	 * @param mixed $pujas
	 */
	public function setPujas($pujas)
	{
		$this->pujas[] = $pujas;
	}



}