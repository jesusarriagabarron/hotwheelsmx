<?php

namespace App\Console;

use App\Http\Models\Subasta;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\App;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->call(function(){
            $subastas = Subasta::getSubastasEnProgreso();

            if($subastas) {
                $ODM = App::make('ODM');
                foreach ($subastas as $subasta) {
                    $finSubasta = $subasta->getFinSubasta();
                    $now = new \DateTime();
                    if ($now > $finSubasta ) {
                        $subasta->setStatus(Subasta::STATUS_FINISHED);
                        $ODM->persist($subasta);
                        $pusher	=	App::make('pusher');
                        $pusher->trigger( 'subasta-'.$subasta->getId().'-channel',
                            'subasta-statusreload',
                            array('reload'=>1));
                    }
                }
                $ODM->flush();
            }

            $subastas = Subasta::getSubastasPorEmpezar();

            if($subastas) {
                $ODM = App::make('ODM');
                foreach ($subastas as $subasta) {
                    $inicioSubasta = $subasta->getInicioSubasta();
                    $now = new \DateTime();
                    if ($now>=$inicioSubasta) {
                        $subasta->setStatus(Subasta::STATUS_IN_PROGRESS);
                        $ODM->persist($subasta);
                        $pusher	=	App::make('pusher');
                        $pusher->trigger( 'subasta-'.$subasta->getId().'-channel',
                            'subasta-statusreload',
                            array('reload'=>1));
                    }
                }
                $ODM->flush();
            }


        })->everyMinute();

    }
}
