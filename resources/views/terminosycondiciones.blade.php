@extends('layouts.master')
@section('title', 'Crear una subasta')




@section('content')
    <div class="container">
        <h1>Términos y condiciones</h1>
        <div class="panel-body text-muted" style="font-size:16px;padding-top:30px;">
            <ol>
                <li>Registrate en la pagina para poder tener acceso a las subasta</li>
                <li>Las subastas son propiedad del subastador</li>
                <li>El subastador define cuando inicia y cuando finaliza una subasta</li>
                <li>Los participantes pueden hacer pujas definidas de acuerdo a las reglas del subastador</li>
                <li>Las subastas inician y finilizan de manera automatica</li>
                <li>La ultima puja es la ganadora al finalizar la subasta no se podran realizar mas pujas</li>
                <li>Cada subasta tiene su id especfico y un area de comentarios de facebook</li>
                <li>La responsabilidad de hacer la entrega y compra del producto recae totalmente entre las partes involucradas
                 el subastador y el ganador definirar el proceso de entrega y pago, sin tener el sitio o sus represantes reponsabilidad alguna</li>
                <li>este sitio solo ofrece el sistema automatica de subasta</li>
            </ol>
        </div>

    </div>
@endsection