<!DOCTYPE html>
<?php
setlocale(LC_MONETARY, 'es_MX');
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="csrf-token" content="{{ csrf_token() }}">


    <meta property="fb:app_id" content="{{env('FB_API_ID')}}" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- FontAwesome CDN CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Google fonts -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,400,700,200italic,700italic|Orbitron:500" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/css/styles.css">

    <link rel='shortcut icon' type='image/x-icon' href='/hwicon.ico'/>
    <title> @yield('title')</title>
    @yield('metadata')
</head>

<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5VM66C"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5VM66C');</script>
<!-- End Google Tag Manager -->
<div id="fb-root"></div>
<!-- Facebook -->
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_MX/sdk.js#xfbml=1&version=v2.6&appId={{env('FB_API_ID')}}";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- /Facebook -->
    <header>
        @include('layouts.menu')
    </header>
    <section>
        @yield('content')
    </section>
    <footer class="footer">
        @include('layouts.footer')
    </footer>



    <div class="modal fade" tabindex="-1" role="dialog" id="smallModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" >
                <div  class="text-muted" style="font-family: 'Oswald',sans-serif;padding:20px;font-size:26px;">
                <span id="smallModalContent">

                </span>
                <span>
                  <img src="/assets/images/loading_dots.gif" width="64" height="64">
                </span>
                </div>

            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- Noty js -->
    <script src="/assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>
    <!-- <script src="/assets/js/noty/themes/relax.js"></script> -->
    <script src="/assets/js/app.js"></script>

    <script src="/assets/js/momentjs/moment-with-locales.min.js"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular-resource.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular-route.min.js"></script>


<script>
    /*global datetime*/
    var d = new Date()
    var n = d.getTimezoneOffset();
    @if(Session::has('notify') )
        <?php $notify = Session::get('notify'); ?>
        var n = noty({
        text: '{{ $notify['text'] }}' ,
        type: '{{ $notify['type'] }}' ,
        timeout:8000,
        theme:'relax',
        layout:'topCenter'
        });
    @endif

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

@yield('javascript')

</body>
</html>