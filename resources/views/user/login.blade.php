@extends('layouts.master')
@section('title', 'Login ')

@section('content')
    <div class="container">
        <div class="initialDiv">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h1 class="text-center"><i class="fa fa-user"></i> Ingresa ahora
                        </h1>
                </div>
            </div>
            <div class="row marginTop-10">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="text-center margin10">
                        <div class="btn-container">
                            <div class="fb-btn" >
                                <a href="{{$loginUrl}}" onclick="smallModal('Cargando')">
                                    <div class="fb-icn"></div>
                                    <p >Login con Facebook</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    @if ($reject_app)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger">
                                    <ul>
                                        {{ $reject_app }}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div style="border-bottom: 1px solid #eee;float: left;margin-top: 10px;width: 45%;"></div>
                    <div style="float:left;margin:0px 10px;color:#aaa"> o </div>
                    <div style="border-bottom: 1px solid #eee;float: left;margin-top: 10px;width: 45%;"></div>
                </div>
            </div>

            @if(false)
            <div class="row marginTop20">
                <div class="col-md-offset-3 col-md-6">
                    <form class="form-horizontal" method="POST" action="{{URL::route('logInAction')}}">
                        {!! csrf_field() !!}
                        @if (count($errors) > 0)
                            <div class="row">
                                <div class="col-md-offset-2 col-md-8">
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="form-group form-group-lg">
                            <label class="col-sm-2 control-label" for="formGroupInputLarge">Email</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" id="formGroupInputLarge" name="email" placeholder="Tu correo electronico" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="form-group form-group-lg">
                            <label class="col-sm-2 control-label" for="formGroupInputLarge">Password</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="password" name="password" id="formGroupInputLarge" placeholder="Ingresa un Password" value="{{ old('password') }}">
                            </div>
                        </div>
                        <p><a href="{{  URL::route('forgotPage')  }}">Olvidaste tu password?</a></p>

                        <div class="form-group form-group-lg ">
                            <div class="col-sm-offset-3 col-sm-6">
                                <input class="form-control btn-success marginTop20" onclick="smallModal('Cargando')" type="submit" name="normalButton" value="Entrar">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            @endif

        </div>
    </div>
@endsection
