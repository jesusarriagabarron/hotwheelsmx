@extends('layouts.master')

@section('content')
<div class="container">
    <div class="initialDiv">
        <div class="row grayContainer">


                <div class="col-md-12">
                    <div class="page-header">
                        <h1 class="text-center"><i class="fa fa-gavel spaceIcon"></i>Lista de Subastas</h1>
                    </div>
                </div>

            @if( $subastas )
                @foreach($subastas as $subasta)
                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <a href="{{ URL::route('verSubasta',array('auto'=>str_slug($subasta->getFirstAuto()->getNombre(),"-"),'id'=> $subasta->getId() )) }}" class="no-decoration">
                            <div class="thumbnail hompage-thumbnail">
                                <!--<div class="ribbon"><span>POPULAR</span></div>-->
                                <img src="/storage/autos/thumb-{{ $subasta->getFirstAuto()->getFirstFoto()->getFilename() }}" class="img-responsive" alt="">
                                <div class="caption homepage-caption">
                                    <h5>{{$subasta->getFirstAuto()->getNombre()}}</h5>
                                    <p class="precio">$ {{ money_format('%i', $subasta->getPrecioInicial())}}</p>
                                    <p class="subastador">{{$subasta->getUsuario()->getFirstname()}}</p>

                                    @if( $subasta->getStatus() == \App\Http\Models\Subasta::STATUS_FINISHED )
                                        <p class="estilo-jeshua">Finalizada</p>
                                    @endif

                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            @endif

           

        </div>
    </div>
</div>

@endsection