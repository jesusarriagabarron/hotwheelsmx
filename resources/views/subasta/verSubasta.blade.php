@extends('layouts.master')
@section('title', 'Crear una subasta')

@section('metadata')
    <meta property="og:url"                content="{{url()->current()}}" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="En Subasta - {{$subasta->getFirstAuto()->getNombre()}}" />
    <meta property="og:description"        content="{{$subasta->getFirstAuto()->getDescripcion()}}" />
    <meta property="og:image"              content="{{url('/storage/autos/'.$subasta->getFirstAuto()->getFirstFoto()->getFilename())}}" />
@endsection

@section('content')
    <div class="container">
        <div class="initialDiv">
            <div class="row grayContainer">
                    <div class="col-md-12">
                        <div class="page-header">
                            <h1 class="text-center">Subasta <br>{{$subasta->getFirstAuto()->getNombre()}}</h1>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-7">
                                    @if($subasta->getStatus()== \App\Http\Models\Subasta::STATUS_CREATED)
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-info">
                                                <h5 class="text-center">La subasta comienza el: <span class="time-format-inicia" time="{{ $subasta->getInicioSubasta()->format('r') }}"></span> </h5>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                        @if( $subasta->getStatus()== \App\Http\Models\Subasta::STATUS_FINISHED || $subasta->getStatus()== \App\Http\Models\Subasta::STATUS_COMPLETED)
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="alert alert-success">
                                                        <h5 class="text-center">La subasta ha terminado, felicidades al ganador</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="row">
                                            <div class="col-md-4 col-xs-6">
                                                <div class="thumbnail text-center">
                                                    <i class="icon-color fa fa-2x fa-gavel spaceIcon"></i>
                                                    <p>Puja más alta</p>
                                                    @if($subasta->getPujaMasAlta() && $subasta->getPujaMasAlta()->getMontoPuja())
                                                        <h3 id="puja-mas-alta" class="title-subasta money">$ {{$subasta->getPujaMasAlta()->getMontoPuja()}}</h3>
                                                    @else
                                                        <h3 id="puja-mas-alta" class="title-subasta money">$ {{$subasta->getPrecioInicial()}}</h3>
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-6">
                                                <div class="thumbnail text-center">
                                                    <i class="icon-color fa fa-users fa-2x" aria-hidden="true"></i>
                                                    <p>Pujas realizadas</p>
                                                    <h3 id="pujas-realizadas" class="title-subasta" >{{ $subasta->getTotalPujas()  }}</h3>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="thumbnail text-center">
                                                    <i class="icon-color fa fa-trophy fa-2x" aria-hidden="true"></i>
                                                    <p>Ganando al momento</p>
                                                    <h3 id="ganador-subasta" class="title-nombre" >
                                                        @if($subasta->getPujaMasAlta())

                                                            @if( \App\Http\Controllers\Auth\AuthMongoController::check())
                                                                @if($subasta->getPujaMasAlta()->getUserPuja()->getId() ==  \App\Http\Controllers\Auth\AuthMongoController::user()->getId())
                                                                    <span style="color:#f00;">Es Tuyo</span>
                                                                @else
                                                                    {{$subasta->getPujaMasAlta()->getUserPuja()->getFirstname()." ".$subasta->getPujaMasAlta()->getUserPuja()->getLastname()}}
                                                                @endif
                                                            @else
                                                                {{$subasta->getPujaMasAlta()->getUserPuja()->getFirstname()." ".$subasta->getPujaMasAlta()->getUserPuja()->getLastname()}}
                                                            @endif

                                                        @else
                                                            -
                                                        @endif
                                                    </h3>
                                                </div>
                                            </div>

                                            <div class="row">
                                                @if(\App\Http\Controllers\Auth\AuthMongoController::check() && $subasta->getStatus() == \App\Http\Models\Subasta::STATUS_IN_PROGRESS )
                                                <div class="col-md-6 col-md-offset-3">
                                                    <div class=" text-center">
                                                            <button id="button-mio" class="btn btn-mio btn-block btn-lg">
                                                                <i class="fa fa-hand-paper-o" aria-hidden="true"></i>
                                                                Mio por
                                                                @if($subasta->getPujaMasAlta() && $subasta->getPujaMasAlta()->getMontoPuja())
                                                                    $<span id="mio-por"> {{$subasta->getPujaMasAlta()->getMontoPuja()+$subasta->getIncrementoPuja()}}</span>
                                                                @else
                                                                    $<span id="mio-por"> {{$subasta->getPrecioInicial()+$subasta->getIncrementoPuja()}}</span>
                                                                @endif
                                                            </button>
                                                    </div>
                                                </div>
                                                @else
                                                    @if( $subasta->getStatus()== \App\Http\Models\Subasta::STATUS_IN_PROGRESS)
                                                        <div class="col-md-10 col-md-offset-1 col-xs-12">
                                                            <div class="well well-lg">
                                                                <p class="text-center registrate">Registrate para participar en la subasta <a href="{{URL::route('signUpPage')}}">aquí</a></p>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 marginTop20">
                                                @if($subasta->getFirstAuto()->getDescripcion())
                                                    <p>{{$subasta->getFirstAuto()->getDescripcion()}}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <p><span class="bold">Finaliza el día: </span><span class="time-format-finaliza" time="{{ $subasta->getFinSubasta()->format('r') }}"></span> </p>
                                        <p><span class="bold">Precio de inicio: </span><span class="money" style="font-weight: bold">$ {{ money_format('%i', $subasta->getPrecioInicial()) }} </span></p>
                                        <p>Puja sin centavos: <span class="money" style="font-weight: bold">$ {{ money_format('%i', $subasta->getIncrementoPuja()) }} </span> </p>
                                        @if($subasta->getReglasAdicionales())
                                        <p>Reglas adicionales: {{ $subasta->getReglasAdicionales()  }}</p>
                                        @endif
                                </div>
                                <div class="col-md-5 marginBottom20 ">
                                    <img class="img-responsive img-rounded" style="margin:0 auto;" src="/storage/autos/{{ $subasta->getFirstAuto()->getFirstFoto()->getFilename() }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="text-center" style="margin-top: 13px; margin-bottom: -10px;">Subastas al momento</h3>
                                <hr>
                                <ul class="media-list">
                                @if($subasta->getTotalPujas()>0)

                                    @foreach($subasta->getPujas(1) as $puja)
                                            <li class="media usuario-puja">
                                                <a href="#">
                                                    <div class="media-left">
                                                        <img class="media-object" src="http://graph.facebook.com/{{$puja->getUserPuja()->getFacebookID()}}/picture?type=small" style="width: 48px;height:auto">
                                                    </div>
                                                    <div class="media-body user-list">
                                                        <h5 class="media-heading time-format" time="{{$puja->getCreatedAtDate()}}"></h5>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <h4>{{$puja->getUserPuja()->getFirstname()." ".$puja->getUserPuja()->getLastname()}}</h4>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h4 class="money">${{$puja->getMontoPuja()}}</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                    @endforeach
                                @else
                                    <p id="sin-subastas" class="text-center alert alert-info">Aún no hay pujas en esta subasta, Sé el primero</p>
                                @endif
                                </ul>

                            </div>
                            <div class="col-md-6">
                                <div class="fb-comments" data-width="100%" data-href="{{url()->current()}}" data-numposts="5"></div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="//js.pusher.com/3.0/pusher.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

<script>

    @if(\App\Http\Controllers\Auth\AuthMongoController::check())
        var id = '{{\App\Http\Controllers\Auth\AuthMongoController::user()->getId()}}';
    @else
        var id = '';
    @endif
    var data = {data: ''};
    var url = "{{URL::route('postMio',array('id'=> $subasta->getId() ))}}";


    $('.time-remaining').each(function(){
        var convertedDate = moment($(this).attr('time'),'ddd, DD MMM YYYY HH:mm:ss ZZ').locale('es').format('D/MMMM/YY h:mm a');
        $(this).text(moment(convertedDate).locale('es').endOf('day').fromNow());
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('#button-mio').click(function(){
        $('#button-mio').attr('disabled','disabled');
        $.post(url, data)
                .success(
                function(response){
                    $('#button-mio').removeAttr('disabled');
                })
                .fail(
                function(response){
                    data = JSON.parse(response.responseText);
                    toastr.error(data.message, null, {"positionClass": "toast-top-right"});
                    if( data.reload && data.reload==1)
                            location.reload();
                    $('#button-mio').removeAttr('disabled');
                });
    });


    $('.time-format').each(function(){
        var localTime  = moment.utc($(this).attr('time')).toDate();
        var convertedDate = moment(localTime).locale('es').format('dddd DD MMMM YYYY, hh:mm:ss a');
        $(this).text(convertedDate);
    });


    var pusher = new Pusher("{{env("PUSHER_KEY")}}");
    var channel = pusher.subscribe('subasta-{{$subasta->getId()}}-channel');


    channel.bind('subasta-statusreload',function(data){
       if ( data.reload == 1 )
           location.reload();
    });

    channel.bind('nueva-puja', function(data) {
        showNotification(data.usuario+' ha realizado una puja por $'+data.montoPuja);
        $('#puja-mas-alta').fadeOut(200,function(){
           $('#puja-mas-alta').text("$ "+data.montoPuja);
            $('#puja-mas-alta').fadeIn();
        });
        $('#pujas-realizadas').fadeOut(200,function(){
            $('#pujas-realizadas').text(data.totalPujas);
            $('#pujas-realizadas').fadeIn();
        });

        $('#ganador-subasta').fadeOut(200,function(){

            if(id != '' && id == data.idUsuario){
                $('#ganador-subasta').html('<span style="color:#f00;">Es Tuyo</span>');
                $('#ganador-subasta').fadeIn();
            }else{
                $('#ganador-subasta').text(data.usuario);
                $('#ganador-subasta').fadeIn();
            }

        });
        $('#mio-por').text(data.mioPor);
        $('#sin-subastas').hide();
        var listaUsuarios = $('.media-list').html();
        $('.media-list').html('<li class="media usuario-puja" style="display: none;"><a href="#"> <div class="media-left"><img class="media-object" src="/assets/images/anonymous_256.png" style="width: 48px;height:auto"> </div> <div class="media-body user-list"> <h5 class="media-heading time-format" time="'+data.timePuja+'"></h5> <div class="row"> <div class="col-md-6"> <h4>'+data.usuario+'</h4> </div> <div class="col-md-6"> <h4 class="money">$'+data.montoPuja+'</h4> </div> </div> </div> </a> </li>'+listaUsuarios);
        var primero = $('.media-list .usuario-puja').first();
        primero.slideDown("slow");
        $('.time-format').each(function(){
            var localTime  = moment.utc($(this).attr('time')).toDate();
            var convertedDate = moment(localTime).locale('es').format('dddd DD MMMM YYYY, hh:mm:ss a');
            $(this).text(convertedDate);
        });
    });

    $('.time-format-finaliza').each(function(){
        var localTime  = moment.utc($(this).attr('time')).toDate();
        var convertedDate = moment(localTime).locale('es').format('dddd DD MMMM YYYY, hh:mm:ss a');
        $(this).text(' '+convertedDate);
    });

    $('.time-format-inicia').each(function(){
        var localTime  = moment.utc($(this).attr('time')).toDate();
        var convertedDate = moment(localTime).locale('es').format('dddd DD MMMM YYYY, hh:mm a');
        $(this).text(' '+convertedDate);
    });

    // Use toastr to show the notification
    function showNotification(text) {
        toastr.success(text, null, {"positionClass": "toast-top-right"});
    }

</script>
@endsection



