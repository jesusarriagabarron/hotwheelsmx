@extends('layouts.master')
@section('title', 'Crear una subasta')

@section('content')
    <div class="container">
        <div class="initialDiv">
            <div class="row grayContainer">
                <div class="col-md-12">
                    <div class="page-header">
                        <h1 class="text-center"><i class="fa fa-gavel spaceIcon"></i>Crear una subasta</h1>
                    </div>
                </div>


                <div class="col-md-12">

                    <form id="form" class="form-horizontal" method="POST" action="{{ URL::route('postCrearSubasta') }}" enctype="multipart/form-data">


                    <div class="row">
                        <!-- Columna 1 -->
                        <div class="col-md-8 ">

                            <h3 class="marginBottom10 marginTop0 text-center">Detalle del auto</h3>
                            <div class="row">
                                <div class="col-md-12">

                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    @if (count($errors) > 0)
                                        <div class="row">
                                            <div class="col-md-offset-2 col-md-8">
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif


                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Modelo de tu Auto</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control input-lg" id="nombre" name="nombre" placeholder="Cual es el modelo o nombre de tu auto" value="{{old('nombre')}}">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Descripción del Auto (opcional)</label>
                                            <div class="col-sm-9 col-xs-12">
                                                <textarea name="descripcion" class="form-control " rows="4" placeholder="Describe las condiciones de tu auto, condiciones del empaque">{{old('descripcion')}}</textarea>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label for="selectMarca" class="col-sm-3 control-label">Marca</label>
                                            <div class="col-sm-9">
                                                <select name="marca" class="form-control input-lg">
                                                    <option value="1"  {{  old('marca')==1?'selected':''  }}>Hot Wheels</option>
                                                    <option value="2"  {{  (int) old('marca')==2?'selected':''  }}>Matchbox</option>
                                                    <option value="3"  {{  (int) old('marca')==3?'selected':''  }}>M2</option>
                                                    <option value="4"  {{  (int) old('marca')==4?'selected':''  }}>Maisto</option>
                                                    <option value="5"  {{  (int) old('marca')==5?'selected':''  }}>Auto World</option>
                                                    <option value="6"  {{  (int) old('marca')==6?'selected':''  }}>Johnny Lightning</option>
                                                    <option value="7"  {{  (int) old('marca')==7?'selected':''  }}>Green light</option>
                                                    <option value="99"  {{  (int) old('marca')==99?'selected':''  }}>Otro</option>
                                                </select>
                                            </div>
                                        </div>

                                    <div class="form-group">
                                        <label for="optionsRadios" class="col-sm-3 control-label">Empaque</label>
                                        <div class="col-sm-9">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="empaque" id="optionsRadios1" value="blister" checked>
                                                    Blister (empaque cerrado)
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="empaque" id="optionsRadios2" value="loose">
                                                    Loose  (sin empaque o abierto)
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Agrega una foto</label>
                                        <div class="col-sm-9 ">
                                            <input id="fileupload" class="margin10" type="file" name="foto"  value="{{old('foto')}}"  accept="image/*">
                                        </div>
                                    </div>





                                    <h3 class="marginBottom10 marginTop0 text-center">Reglas</h3>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Inicio de la Subasta</label>
                                        <div class="col-md-9 ">
                                            <input name="inicioSubastaMobile" class="form-control input-lg" type="datetime-local" value="{{old('inicioSubastaMobile')}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Fin de la Subasta</label>
                                        <div class="col-sm-9 ">
                                            <input name="finSubastaMobile" class="form-control input-lg" type="datetime-local" value="{{old('finSubastaMobile')}}">
                                        </div>
                                    </div>

                                    <input type="hidden" id="inicioDateTime" name="inicioDateTime" value="{{old('inicioDateTime')}}">
                                    <input type="hidden" id="finDateTime" name="finDateTime" value="{{old('finDateTime')}}">


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Monto Inicial $</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-lg money" name="precioInicial" placeholder="Precio Inicial" value="{{old('precioInicial')}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Puja</label>
                                        <div class="col-sm-9">
                                            <select name="tipoPuja" class="form-control input-lg" >
                                                @if(false)
                                                <option value="1" {{  old('tipoPuja')==1?'selected':''  }}>Libre (la puja es libre)</option>
                                                @endif
                                                <option value="2" selected {{  old('tipoPuja')==2?'selected':''  }}>Fija (tu defines el incremento)</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group div-incremento">
                                        <label class="col-sm-3 control-label">Incremento x Puja $</label>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control money" name="incrementoPuja" value="{{old('incrementoPuja')}}">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Reglas adicionales (opcional)</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <textarea name="reglas" class="form-control" rows="6" placeholder="Escribe tus reglas personales o detalles de tu subasta">{{old('reglas')}}</textarea>
                                        </div>
                                    </div>






                                </div>
                            </div>
                        </div>
                        <!-- Columna 2 -->
                        <div class="col-md-5">


                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-offset-4 col-md-4 col-xs-offset-1 col-xs-10">
                                    <button id="buttonSubmit" type="submit" class="btn btn-success btn-lg btn-block"><i class="fa fa-save spaceIcon"></i> Guardar </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    </form>

                </div>
                <!-- End col-md-12 principal -->
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="/assets/js/jquery-file/js/vendor/jquery.ui.widget.js"></script>
    <script src="/assets/js/jquery-file/js/jquery.iframe-transport.js"></script>
    <script src="/assets/js/jquery-file/js/jquery.fileupload.js"></script>
    <script src="/assets/js/jquery-file/js/load-image.all.min.js"></script>
    <script src="/assets/js/jquery-file/js/canvas-to-blob.min.js"></script>
    <script src="/assets/js/jquery-file/js/jquery.fileupload-process.js"></script>

    <script src="/assets/js/jquery-file/js/jquery.fileupload-image.js"></script>
    <script src="/assets/js/jquery-file/js/jquery.fileupload-validate.js"></script>


    <!-- ... -->
    <script type="text/javascript" src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />




    <script>
        $(document).ready(function(){
            'use strict';


            $('#buttonSubmit').click(function(e){
                e.preventDefault();
                smallModal('Cargando');
                if(validateForm()){
                    $('#form').submit();
                }else{
                    smallModalHide();
                }
            });


            function validateForm(){
                var hasError = false;

                $('.errorBorder').each(function(){
                    $(this).removeClass('errorBorder');
                });

                if(!$('#nombre').val().length){
                    $('#nombre').addClass('errorBorder');
                    hasError = true;
                }

                if(!$('input[name="inicioSubastaMobile"]').val().length){
                    $('input[name="inicioSubastaMobile"]').addClass('errorBorder');
                    hasError = true;
                }

                if(!$('input[name="finSubastaMobile"]').val().length){
                    $('input[name="finSubastaMobile"]').addClass('errorBorder');
                    hasError = true;
                }

                if(parseInt($('select[name="tipoPuja"]').val())==2  && parseInt($('input[name="incrementoPuja"]').val())<1  || isNaN(parseInt($('input[name="incrementoPuja"]').val()))==true){
                    $('input[name="incrementoPuja"]').addClass('errorBorder');
                    hasError    =   true;
                }



                if(hasError){
                    return false;
                }

                return true;
            }


            $('.time-lastcars').each(function(){
                var convertedDate = moment($(this).attr('time'),'ddd, DD MMM YYYY HH:mm:ss ZZ').locale('es').format('D/MMMM/YY h:mm a');
                $(this).text(convertedDate);
            });




            $('input[name=inicioSubastaMobile]').change(function(){
                var fechaInicioM = moment($('input[name=inicioSubastaMobile]').val());
                $('#inicioDateTime').val(fechaInicioM.utc().format());
                console.log($('#inicioDateTime').val());
            });
            $('input[name=finSubastaMobile]').change(function(){
                var fechaFinM = moment($('input[name=finSubastaMobile]').val());
                $('#finDateTime').val(fechaFinM.utc().format());
                console.log($('#finDateTime').val());
            });



            function checkIncremento(){
                if( parseInt($('select[name=tipoPuja]').val()) == 2 ){
                    $('.div-incremento').slideDown();
                }else{
                    $('.div-incremento').slideUp();
                }
            }

            $('select[name=tipoPuja]').change(function(){
                checkIncremento();
            });
            checkIncremento();


        })


    </script>

@endsection