<!DOCTYPE html>
<html>
<head>
    <title>Error la p&aacutegina no existe</title>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- FontAwesome CDN CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Google fonts -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,400,700,200italic,700italic|Orbitron:500" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/css/styles.css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            font-weight: 100;
            font-family: 'Source Sans Pro', sans-serif;
        }


        .title {
            font-family: 'Orbitron', sans-serif;
            margin-top: 20%;
            font-size: 30px;
            margin-bottom: 40px;
        }
    </style>
</head>
<body>
<header>
    @include('layouts.menu')
</header>
<div class="container">
    <div class="initialDiv">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <div class="title">Parece que est&aacutes perdido<br><small> ERROR 404</small><br><br>
                    <i class="fa fa-bomb fa-2x"></i>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</body>
</html>
