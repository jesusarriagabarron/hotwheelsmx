@extends('layouts.master')
@section('title', 'Crear una subasta')




@section('content')
<div class="container">

   <style>
       .c7{
           font-weight: bold;
       }
   </style>
    <h1>Aviso de privacidad</h1>
    <p class="c0">
        <span class="c4 c7">I. IDENTIDAD Y DOMICILIO DEL RESPONSABLE.</span><span class="c4">&nbsp;
El presente Aviso de Privacidad (en lo sucesivo referido como “Aviso”) establece los términos
y condiciones en virtud de los cuales JESUS ARRIAGA BARRON (en adelante “Hotwheelsmx.com”)
 con domicilio en 5624 Lake District Drive, The Colony Texas Zipcode 75056;
 en su carácter de Responsable tratará los datos personales del Titular.</span></p>
    <p class="c0">
        <span class="c4">&nbsp;</span></p>
    <p class="c0"><span class="c4 c7">II. CONSENTIMIENTO DEL TITULAR.</span>
 <span class="c4">&nbsp;Para efectos de lo dispuesto en la Ley Federal de Protección de
 Datos Personales en Posesión de los Particulares y demás legislación aplicable,
 el Titular manifiesta (i) que el presente Aviso le ha sido dado a conocer por el Responsable en tiempo y forma, (ii)
  haber leído, entendido y acordado los términos expuestos en este Aviso de privacidad a entera
  satisfacción, por lo que otorga su consentimiento respecto del tratamiento de sus datos personales
  recabados por el responsable. En caso de que los datos personales recopilados incluyan datos patrimoniales o
  financieros, mediante la firma del contrato correspondiente, sea en formato impreso, o utilizando medios
  electrónicos y sus correspondientes procesos para la formación del consentimiento,
  se llevarán a cabo actos que constituyen el consentimiento expreso del titular y (iii)
  que otorga su consentimiento para que Hotwheelsmx.com y sus Encargados realicen transferencias
   y/o remisiones de datos personales en términos del apartado V del presente Aviso.</span></p>
    <p class="c0"><span class="c4">&nbsp;</span></p>
    <p class="c0"><span class="c4 c7">III. DATOS PERSONALES QUE RECABAMOS.
   </span><span class="c4">&nbsp;Hotwheelsmx.com puede recolectar datos personales del Titular mediante la
   entrega directa y/o personal por cualquier medio de contacto entre el Titular y el Responsable o sus Encargados.</span>
    </p>
    <p class="c0"><span class="c4">Hotwheelsmx.com recaba datos personales de identificación y/o
   financieros, así como datos relacionados con la prestación de los servicios de mediación y datos referentes
   al acceso y/o uso de dichos servicios.</span></p>
    <p class="c0"><span class="c4">&nbsp;</span></p>
    <p class="c0">
        <span class="c4 c7">IV. FINALIDAD DE LOS DATOS PERSONALES.</span><span class="c3">&nbsp;</span><span class="c4">
   FINALIDAD PRIMARIA: </span><span class="c4">Hotwheelsmx.com</span>
   <span class="c4">&nbsp;tratará los datos personales del Titular con la finalidad de llevar a cabo
   las actividades y gestiones enfocadas al cumplimiento de las obligaciones originadas y derivadas de cualquier
   relación jurídica y comercial que establezcamos con motivo de la prestación de nuestros
   servicios; facturación; cobranza; crédito; atención a clientes; servicio técnico;
    otorgamiento de garantías; gestión de servicios de valor agregado y contenidos; reciclaje de
    terminales; administración de aplicaciones y sitios Web; contacto con el cliente, con la fuerza de ventas
     y distribuidores; y proporcionar, renovar, cambiar o cancelar los servicios que nos solicita el Titular.</span>
    </p>
    <p class="c0"><span class="c4">&nbsp;</span></p>
    <p class="c0"><span class="c4">FINALIDAD SECUNDARIA: Asimismo,
      Hotwheelsmx.com tratará datos personales para otras finalidades como enviar notificación de
       ofertas, avisos y/o mensajes promocionales; comunicaciones con fines de mercadotecnia, publicitarios o telemarketing
       sobre productos y servicios nuevos o existentes; realizar encuestas; estadísticas; estudios de mercado, sobre
        hábitos de consumo, intereses y comportamiento; realizar programas de beneficios e incentivos; participar en
         redes sociales, chats y/o foros de discusión; participar en eventos, trivias, concursos, rifas, juegos y
          sorteos; evaluar la calidad de los servicios; y en general para cualquier actividad encaminada a promover,
          mantener, mejorar y evaluar nuestros productos y servicios.</span></p>
    <p class="c0 c2"><span class="c4"></span>
    </p>
    <p class="c0"><span class="c4">Puede oponerse al tratamiento de sus datos para las finalidades secundarias
           en cualquier momento a través de los medios que hemos puesto a su disposición para el ejercicio de
           sus derechos ARCO. En caso de no oponerse al tratamiento de sus datos personales para las finalidades secundarias
          en un plazo de cinco días hábiles posteriores a que sus datos fueron recabados, se entenderá que ha otorgado su consentimiento.</span>
    </p>
    <p class="c0"><span class="c4">&nbsp;</span></p>
    <p class="c0"><span class="c4 c7">V. TRANSFERENCIAS Y/O REMISIONES DE DATOS.
</span><span class="c4">&nbsp;Hotwheelsmx.com requiere compartir los datos personales del Titular con el objeto de dar
cumplimiento a sus obligaciones jurídicas y/o comerciales, para lo cual ha celebrado o celebrará diversos
acuerdos comerciales tanto en territorio nacional como en el extranjero. Los receptores de los datos personales,
 están obligados por virtud del contrato correspondiente, a mantener la confidencialidad de los datos personales
suministrados por Hotwheelsmx.com y a observar el presente Aviso. Hotwheelsmx.com y/o sus Encargados
podrán transferir los datos personales recolectados del Titular a cualquier otra sociedad del mismo grupo empresarial
al que pertenezca el Responsable y que operen con los mismos procesos y políticas internas, sea que se encuentren en
territorio nacional o en el extranjero para su tratamiento con las mismas finalidades descritas en este Aviso.</span>
    </p>
    <p class="c0 c2"><span class="c4"></span></p>
    <p class="c0"><span class="c4">Su información personal puede
transferirse, almacenarse y procesarse en un país distinto de donde se proporcionó. Sí lo hacemos,
transferimos la información de conformidad con las leyes de protección de la información aplicables.
Tomamos medidas para proteger la información personal sin importar el país donde se almacena o a donde se
transfiere. Tenemos procedimientos y controles oportunos para procurar esta protección.</span></p>
    <p class="c0">
        <span class="c4">&nbsp;</span></p>
    <p class="c0"><span class="c4 c7">VI. PROCEDIMIENTO PARA EJERCER LOS DERECHOS ARCO Y
REVOCACIÓN DEL CONSENTIMIENTO.</span><span class="c4">&nbsp;</span><span class="c4">El Titular tiene, en todo momento,
derecho de acceder, rectificar y cancelar sus datos, así como de oponerse al tratamiento de los mismos o revocar
el consentimiento que para tal fin haya proporcionado presentando una solicitud en el formato que para tal fin le
entregaremos a petición expresa del titular, misma que debe contener la información y documentación
siguiente:</span></p>
    <p class="c0"><span class="c4">&nbsp;</span></p>
    <p class="c0"><span class="c4">
i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nombre del Titular y domicilio u otro medio para comunicarle la respuesta
a su solicitud;</span></p>
    <p class="c0"><span class="c4">ii)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Los documentos
que acrediten su identidad (copia simple en formato impreso o electrónico de su credencial de elector, pasaporte o
FM-3) o, en su caso, la representación legal del Titular (copia simple en formato impreso o electrónico de la
carta poder simple con firma autógrafa del Titular, el mandatario y sus correspondientes identificaciones
oficiales – credencial de elector, pasaporte o FM-3);</span></p>
    <p class="c0"><span class="c4">iii)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La descripción
clara y precisa de los datos personales respecto de los que busca ejercer alguno de los Derechos ARCO, y</span></p>
    <p class="c0"><span class="c4">iv)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cualquier otro elemento o documento que
facilite la localización de los datos personales del Titular.</span></p>
    <p class="c0"><span class="c4 c8">&nbsp;</span>
    </p>
    <p class="c0"><span class="c4">En el caso de las solicitudes de rectificación de datos personales, el Titular
deberá también indicar las modificaciones a realizarse y aportar la documentación que sustente su
petición.</span></p>
    <p class="c0 c2"><span class="c4"></span></p>
    <p class="c0"><span class="c4">Para dar cumplimiento
a la obligación de acceso a sus datos personales, se hará previa acreditación de la identidad del
titular o personalidad del representante; poniendo la información a disposición en sitio ya en el domicilio
del Responsable. Se podrá acordar otro medio entre el Titular y el Responsable siempre que la información
solicitada así lo permita.</span></p>
    <p class="c0 c2"><span class="c4"></span></p>
    <p class="c0"><span class="c4">
Para la petición del formato, recepción, registro, atención y respuesta de las solicitudes para ejercer
sus derechos ARCO, la revocación de su consentimiento y los demás derechos previstos en la LFPDP ponemos a su
disposición los siguientes medios:</span></p>
    <p class="c0 c2"><span class="c4"></span></p><a href="#" name="c155a22d4dbf65965a3b506ebc089ade78da5895"></a><a
            href="#" name="0"></a>
    <table cellpadding="0" cellspacing="0" class="c12">
        <tbody>
        <tr class="c9">
            <td class="c5"><p class="c10 c2"><span class="c4"></span></p></td>
            <td class="c1">
                <p class="c10 c2"><span class="c4"></span></p></td>
        </tr>
        <tr>
            <td class="c5"><p class="c10"><span class="c4">
cancelacion@hotwheelsmx.com</span></p></td>
            <td class="c1"><p class="c2 c10"><span class="c4"></span></p>
            </td>
        </tr>
        <tr>
            <td class="c5"><p class="c10 c2"><span class="c4"></span></p></td>
            <td class="c1"><p class="c10 c2">
                    <span class="c4"></span></p></td>
        </tr>
        </tbody>
    </table>
    <p class="c0 c2"><span class="c4"></span></p>
    <p class="c0">
<span class="c4">En caso de que la información proporcionada en su formato de solicitud sea errónea o
insuficiente, o bien, no se acompañen los documentos de acreditación correspondientes, podremos solicitarle,
dentro de los cinco días hábiles siguientes a la recepción de la solicitud, que aporte los elementos
o documentos necesarios para dar trámite a la misma. El Titular contará con diez días hábiles
para atender el requerimiento, contados a partir del día siguiente en que lo haya recibido. De no dar respuesta en
dicho plazo, se tendrá por no presentada la solicitud correspondiente.</span></p>
    <p class="c0 c2"><span class="c4">
</span></p>
    <p class="c0"><span class="c4">Hotwheelsmx.com o sus Encargados le responderán al Titular
respectivo en un plazo máximo de veinte días hábiles, contados desde la fecha en que se recibió
la solicitud a efecto de que, si resulta procedente, El Responsable haga efectiva la misma dentro de los quince
días hábiles siguientes a que se comunique la respuesta. En todos los casos, la respuesta se dará
por la misma vía por la que haya presentado su solicitud o en su caso por cualquier otro medio acordado con el
Titular. Los plazos antes referidos podrán ser ampliados en términos de la LFPDP.</span></p>
    <p class="c0">
        <span class="c4">&nbsp;</span></p>
    <p class="c0"><span class="c4 c7">VII. LIMITACIÓN DE USO Y DIVULGACIÓN DE
LA INFORMACIÓN.</span><span class="c4">&nbsp;</span><span class="c4">El Responsable y/o sus Encargados
conservarán los datos personales del Titular durante el tiempo que sea necesario para procesar sus solicitudes
de información, productos y/o servicios, así como para mantener los registros contables, financieros y de
auditoria en términos de la LFPDP y de la legislación mercantil, fiscal y administrativa vigente.
Además de controlar la divulgación de dichos datos o información de socios comerciales.</span></p>
    <p class="c0 c2"><span class="c4"></span></p>
    <p class="c0"><span class="c4">Los datos personales recolectados se
encontrarán protegidos por medidas de seguridad administrativas, técnicas y físicas adecuadas
contra el daño, pérdida, alteración, destrucción o uso, acceso o tratamiento no autorizados,
de conformidad con lo dispuesto en la LFPDP y la demás legislación aplicable. No obstante lo señalado
anteriormente, Hotwheelsmx.com no garantiza que terceros no autorizados no puedan tener acceso a los
sistemas físicos o lógicos de los Titulares o del Responsable o en los documentos electrónicos
y archivos almacenados en sus sistemas. En consecuencia, Hotwheelsmx.com no será en ningún caso
responsable de los daños y perjuicios que pudieran derivarse de dicho acceso no autorizado.</span></p>
    <p class="c0 c2"><span class="c4"></span></p>
    <p class="c0"><span class="c4">Usted o su representante legal debidamente
acreditado podrán limitar el uso o divulgación de sus datos personales a través de los mismos medios
y procedimientos dispuestos para el ejercicio de los Derechos ARCO. Una vez que su solicitud resulte procedente,
será registrado en el listado de exclusión dispuesto por Hotwheelsmx.com con la finalidad de que el
Titular deje de recibir información relativa a campañas publicitarias o de mercadotecnia.</span></p>
    <p class="c0 c2"><span class="c4"></span></p>
    <p class="c0"><span class="c4">En caso de que usted considere que
Hotwheelsmx.com ha vulnerado su derecho a la protección de sus datos personales, usted tiene el
derecho de acudir al Instituto Federal de Acceso a la Información y Protección de Datos (“IFAI”).
</span></p>
    <p class="c0"><span class="c4">&nbsp;</span></p>
    <p class="c0"><span class="c4 c7">VIII. RECOLECCIÓN
DE DATOS AL NAVEGAR EN SITIOS Y PÁGINAS WEB DE Hotwheelsmx.com.</span><span class="c4">&nbsp;
Hotwheelsmx.com puede recabar datos a través de sus sitios Web, o mediante el uso de herramientas de
captura automática de datos. Dichas herramientas le permiten recolectar la información que envía
su navegador a dichos sitios Web, tales como el tipo de navegador que utiliza, el idioma de usuario, los tiempos de acceso,
y la dirección IP de sitios Web que utilizó para acceder a los sitios del Responsable o sus Encargados.
</span></p>
    <p class="c0 c2"><span class="c4"></span></p>
    <p class="c0"><span class="c4">Dentro de las herramientas de
captura automática de datos utilizadas por Hotwheelsmx.com en sus sitios y páginas web se encuentran
las cookies, los Web beacons, y los enlaces en los correos electrónicos.</span></p>
    <p class="c0 c2"><span class="c4">
</span></p>
    <p class="c0"><span class="c4 c8">Uso de Cookies.- </span><span class="c4">El correcto funcionamiento de los
sitios de Hotwheelsmx.com requieren de la habilitación de “cookies” en su navegador de Internet.
Las "cookies" son pequeños archivos de datos transferidos por el sitio Web al disco duro de su computadora
cuando navega por el sitio. En la mayoría de los navegadores las cookies se aceptan automáticamente en virtud
de su configuración predeterminada, usted puede ajustar las preferencias de su navegador para aceptar o rechazar
las cookies. La desactivación de las cookies puede inhabilitar diversas funciones de los sitios web de
Hotwheelsmx.com o que no se muestren correctamente. En caso de que usted prefiera eliminar las cookies,
usted puede eliminar el archivo al final de cada sesión del explorador.</span></p>
    <p class="c0 c2"><span class="c4">
</span></p>
    <p class="c0"><span class="c4 c8">Uso de Web beacons.- </span><span class="c4">También conocidos como
etiquetas de Internet, etiquetas de píxel y clear GIFs).- Hotwheelsmx.com puede utilizar en su sitios web y
en sus correos electrónicos con formato HTML los Web beacons, solos o en combinación con las cookies,
para recopilar información sobre el uso de los sitios web y su interacción con el correo electrónico.
El Web beacon es una imagen electrónica, llamada de un solo píxel (1x1) o GIF que puede reconocer
información que es procesada en su computadora, como el caso de las cookies, la hora y fecha en que el sitio
y sus secciones son visualizados.</span></p>
    <p class="c0 c2"><span class="c4"></span></p>
    <p class="c0"><span class="c4 c8">
Vínculos en los correos electrónicos de Hotwheelsmx.com.- </span><span class="c4">Los correos
electrónicos que incluyen vínculos que permiten a Hotwheelsmx.com saber si usted activó
dicho vínculo y visitó la página web de destino, pudiendo esta información ser incluida en su
perfil. Los correos electrónicos de Hotwheelsmx.com pueden incluir vínculos diseñados para
dirigirlo a las secciones relevantes de los sitios Web, al re-direccionarlo a través de los servidores de
Hotwheelsmx.com. El sistema de re-direccionamiento permite a determinar la eficacia de sus campañas de
marketing en línea.</span></p>
    <p class="c0 c2"><span class="c4"></span></p>
    <p class="c0"><span class="c4 c8">
Protección a menores, a personas en estado de interdicción o incapacidad</span><span class="c4 c7">: </span>
        <span class="c4">Hotwheelsmx.com</span><span class="c4">&nbsp;alienta a los padres y/o tutores a tomar un papel
activo en las actividades en línea de sus hijos o representados. En caso de que Hotwheelsmx.com considere
que los datos personales han sido proporcionados por un menor o por una persona en estado de interdicción o
incapacidad, en contravención al presente Aviso, Hotwheelsmx.com procederá a eliminar tales datos
personales a la brevedad. Si usted se da cuenta que tales datos personales han sido proporcionados por un menor de edad o
por una persona en estado de interdicción o incapacidad, por favor envíe un correo electrónico a la
dirección: cancelacion@hotwheelsmx.com</span></p>
    <p class="c0"><span class="c4">&nbsp;</span></p>
    <p class="c0"><span class="c4 c7">IX. CAMBIOS AL AVISO.</span><span class="c4">&nbsp;Hotwheelsmx.com se reserva
el derecho de actualizar periódicamente el presente Aviso para reflejar los cambios en nuestras prácticas
de información. Es responsabilidad del Titular revisar el contenido del Aviso en el sitio &nbsp;</span><span
                class="c14">http://www.hotwheelsmx.com.mx/politica-de-privacidad</span><span class="c4">&nbsp;El Responsable
entenderá que de no expresar lo contrario, significa que el Titular ha leído, entendido y acordado
los términos ahí expuestos, lo que constituye su consentimiento a los cambios y/o actualizaciones respecto
al tratamiento de sus datos personales.</span></p>
    <p class="c2 c15"><span></span></p>
</div>
@endsection